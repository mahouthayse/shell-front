const INITIAL_STATE = {
    well: {
        engineer: '',
        field: '',
        joinVenture: '',
        genericModel: '',
        tvd: '',
        md: '',
        planningStart: '',
        drillingStart: '',
        endDate: '',
        equipments: [],
    },
    message: '',
};

export default function well(state = INITIAL_STATE, action){
    switch(action.type) {
        case "setEngineer":
            return {
                ...state,
                well: {...state.well, engineer: action.engineer},
            }

        case "setField":
            return {
                ...state,
                well: {...state.well, field: action.field},
            }

        case "setEquipments":
            return {
                ...state,
                well: {...state.well, equipments: action.equipments},
            }



        case "setJoinVenture":
            return {
                ...state,
                well: {...state.well, joinVenture: action.joinVenture},
            }


        case "setGenericModel":
            return {
                ...state,
                well: {...state.well, genericModel: action.genericModel},
            }

        case "setTvd":
            return {
                ...state,
                well: {...state.well, tvd: action.tvd},
            }

        case "setMd":
            return {
                ...state,
                well: {...state.well, md: action.md},
            }


        case "setPlanningStart":
            return {
                ...state,
                well: {...state.well, planningStart: action.planningStart},
            }

        case "setDrillingStart":
            return {
                ...state,
                well: {...state.well, drillingStart: action.drillingStart},
            }

        case "setEndDate":
            return {
                ...state,
                well: {...state.well, endDate: action.endDate},
            }

        default:
            return state
    }
}
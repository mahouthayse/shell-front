import {combineReducers} from "redux";
import user from "./user";
import well from "./well"
import equipment from "./equipment";

const reducers = combineReducers({
    user,
    well,
    equipment

});

export default reducers;
import {useState} from "react";

const INITIAL_STATE = {
    equipment: {
        equipmentName: '',
        length: '',
        internalDiameter: '',
        externalDiameter: '',
        grade: '',
        weight: '',
        materialType: ''


    },
    message: '',
};

export default function equipment(state = INITIAL_STATE, action){
    switch(action.type) {

        case "setEquipmentName":
            return {
                ...state,
                equipment: {...state.equipment, equipmentName: action.equipmentName},
            }

        case "setLength":
            return {
                ...state,
                equipment: {...state.equipment, length: action.length},
            }

        case "setInternalDiameter":
            return {
                ...state,
                equipment: {...state.equipment, internalDiameter: action.internalDiameter},
            }

        case "setExternalDiameter":
            return {
                ...state,
                equipment: {...state.equipment, externalDiameter: action.externalDiameter},
            }

        case "setWeight":
            return {
                ...state,
                equipment: {...state.equipment, weight: action.weight},
            }

        case "setGrade":
            return {
                ...state,
                equipment: {...state.equipment, grade: action.grade},
            }

        case "setMaterialType":
            return {
                ...state,
                equipment: {...state.equipment, materialType: action.materialType},
            }

        default:
            return state
    }
}
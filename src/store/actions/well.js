const actions = {

    setEngineer: function(engineer) {
        return {
            type: "setEngineer",
            engineer
        };
    },
    setField: function(field) {
        return {
            type: "setField",
            field
        };
    },
    setJoinVenture: function(joinVenture) {
        return {
            type: "setJoinVenture",
            joinVenture
        };
    },

    setGenericModel: function(genericModel) {
        return {
            type: "setGenericModel",
            genericModel
        };
    },


    setEquipments: function(equipments) {
        return {
            type: "setEquipments",
            equipments
        };
    },



    setTvd: function(tvd) {
        return {
            type: "setTvd",
            tvd
        };
    },

    setMd: function(md) {
        return {
            type: "setMd",
            md
        };
    },


    setPlanningStart: function(planningStart) {
        return {
            type: "setPlanningStart",
            planningStart
        };
    },

    setDrillingStart: function(drillingStart) {
        return {
            type: "setDrillingStart",
            drillingStart
        };
    },

    setEndDate: function(endDate) {
        return {
            type: "setEndDate",
            endDate
        };
    },




};

export default actions;
const actions = {

    setEquipmentName: function(equipmentName) {
        return {
            type: "setEquipmentName",
            equipmentName
        };
    },

    setLength: function(length) {
        return {
            type: "setLength",
            length
        };
    },

    setInternalDiameter: function(internalDiameter) {
        return {
            type: "setInternalDiameter",
            internalDiameter
        };
    },

    setExternalDiameter: function(externalDiameter) {
        return {
            type: "setExternalDiameter",
            externalDiameter
        };
    },

    setWeight: function(weight) {
        return {
            type: "setWeight",
            weight
        };
    },


    setGrade: function(grade) {
        return {
            type: "setGrade",
            grade
        };
    },


    setMaterialType: function(materialType) {
        return {
            type: "setMaterialType",
            materialType
        };
    },




};

export default actions;
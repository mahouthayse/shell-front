import React from "react";
import "./style.scss";
import { Grid, ListItemText, List, ListItem, ListItemIcon} from "@material-ui/core";
import Logo from "../../../assets/logosmall.png";
import { Link } from 'react-router-dom';
import DashboardIcon from '@material-ui/icons/Dashboard';
import StoreIcon from '@material-ui/icons/Store';
import PostAddIcon from '@material-ui/icons/PostAdd';

export default function Menu(){
    return(
        <Grid container className="menu-container" xs={12} direction="row" alignItems="flex-start">

            <Grid container className="menu-header" xs={12} direction="column" justify="center" alignItems="center">
                <img src={Logo}/>
            </Grid>

            <Grid container className="menu-main" xs={12} direction="column" justify="flex-start" alignItems="flex-start">
                <List style={{width:'100%'}}>

                    <Link to="/dashboard" className="menu-link-primario">
                        <ListItem button>
                            <ListItemIcon>
                                <DashboardIcon/>
                            </ListItemIcon>
                            <ListItemText>
                                Dashboard
                            </ListItemText>
                        </ListItem>
                    </Link>

                    <Link to="/equipments" className="menu-link-primario">
                        <ListItem button>
                            <ListItemIcon>
                                <PostAddIcon/>
                            </ListItemIcon>
                            <ListItemText>
                                My Equipments
                            </ListItemText>
                        </ListItem>
                    </Link>

                   <Link to="/select-well" className="menu-link-primario">
                       <ListItem button>
                        <ListItemIcon>
                            <StoreIcon/>
                        </ListItemIcon>
                        <ListItemText>
                            New Well
                        </ListItemText>
                    </ListItem>
                   </Link>

                </List>
            </Grid>

            <Grid container className="menu-footer" xs={12} direction="row" justify="flex-start" alignItems="flex-end">
                <p style={{fontSize: '1.25rem', color:'#9E241C', width:'100%'}}> <i className="fas fa-headset"></i> <strong>Support</strong></p>
                <p style={{width:'100%'}}>contact@i2wm.com</p>
                <p style={{width:'100%'}}>+8801829193637</p>
                <p style={{color:'#9E241C', width:'100%'}}>www.i2wm.com</p>
            </Grid>

        </Grid>

    )
}
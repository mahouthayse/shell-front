import React from "react";
import {Grid, Hidden} from "@material-ui/core";
import Menu from "../Menu";
import "./style.scss";
import HeaderAppbar from "../Appbar";


export default class Base extends React.Component {
    render(){

        return(
            <Grid container className="base-dashboard" xs={12} lg={12} direction='row'>

                <Hidden smDown>
                <Grid item className="menu-dashboard" md={2}>
                    <Menu history={this.props.history}/>
                </Grid>
                </Hidden>

                <Grid container className="main-content" xs={12} md={10}>

                    <Grid item className="appbar" xs={12}>
                       <HeaderAppbar/>
                    </Grid>

                <Grid item className="main-dashboard" xs={12}>
                    {this.props.children}
                </Grid>

                </Grid>

            </Grid>
        );
    }


}
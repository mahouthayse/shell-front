import React from 'react';
import {Grid, Avatar} from "@material-ui/core";
import "./style.scss";


export default function HeaderAppbar() {

    return (
      <Grid container className="appbar-container" xs={12} direction="row" justify="space-around" alignItems="center">
          <Grid container className="appbar-search-container" xs={12} md={10} direction="row" justify="flex-start" alignItems="center">
              <input type="search" placeholder="Search for anything"/>
              <i className="fas fa-search fa-lg"/>
          </Grid>

          <Grid container className="appbar-icons-container" xs={12} md={1} direction="row" justify="space-around" alignItems="center">
              <i className="fas fa-bell fa-lg"/>
              <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
          </Grid>



      </Grid>
    );
}
import React, { useEffect } from 'react';
import "./style.scss";
import {Grid} from "@material-ui/core";
import { Chart } from "react-google-charts";

export default function BarChart(){
    return(
        <Grid container className="barchart-wrapper" xs={12}>
            <Chart
                width={'100%'}
                height={'400px'}
                chartType="Gantt"
                loader={<div>Loading Chart</div>}
                data={[
                    [
                        { type: 'string', label: 'Task ID' },
                        { type: 'string', label: 'Task Name' },
                        { type: 'string', label: 'Resource' },
                        { type: 'date', label: 'Start Date' },
                        { type: 'date', label: 'End Date' },
                        { type: 'number', label: 'Duration' },
                        { type: 'number', label: 'Percent Complete' },
                        { type: 'string', label: 'Dependencies' },
                    ],
                    [
                        'Drill Ship',
                        'Drill Ship',
                        'Drill Ship',
                        new Date(2020, 1, 1),
                        new Date(2020, 6, 20),
                        null,
                        100,
                        null,
                    ],
                    [
                        'Backups',
                        'Backups',
                        'Backups',
                        new Date(2020, 1, 1),
                        new Date(2020, 12, 31),
                        null,
                        100,
                        null
                    ],
                    [
                        'Semi Submersible',
                        'Semi Submersible',
                        'Semi Submersible',
                        new Date(2020, 7, 27),
                        new Date(2020, 12, 31),
                        null,
                        100,
                        null,
                    ],
                    [
                        'Platform',
                        'Platform',
                        'Platform',
                        new Date(2020, 1, 1),
                        new Date(2020, 12, 31),
                        null,
                        100,
                        null,
                    ]
                ]}
                options={{
                    height: 400,
                    gantt: {
                        trackHeight: 85,
                    },
                }}
                rootProps={{ 'data-testid': '2' }}
            />
        </Grid>
    )
}
import React, {useEffect, useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Grid, ListItem, ListItemText, List, MenuItem} from "@material-ui/core";
import { Scrollbars } from 'react-custom-scrollbars';
import "./style.scss";
import api from "../../services/api";

export default function SelectModel(){
    const [model, setModel] = useState([]);
    let modelo = [];

    async function getModels(){
        const response = api.get('/generic-models/').then(response => {

            response.data.map( index =>{
                modelo.push(index);

            });
            setModel(modelo);
        })
        console.log(model)
        console.log(modelo)
    }

    useEffect(() => {
        getModels()
    }, []);

    function handleChange(id){
        localStorage.setItem('modelId', id)
    }


    return(

        <Grid container xs={12} sm={5} direction='row' alignItems='flex-start' justify='flex-start' className='select-container'>
            <Grid container className='header' xs={12} direction='row' alignItems='center' justify='space-between'>
                <h2> Generic Model</h2>
                <i className="fas fa-chevron-up fa-2x" style={{color: '#979797'}}/>
            </Grid>

            <Grid container className='content' xs={12} direction='row' alignItems='center' justify='space-around'>
                <Scrollbars style={{height: 150 }}>
                    <List style={{width: '100%'}}>

                        {model.map((item) => (
                            <ListItem button key={item.id} onClick={ () => handleChange(item.id)}>
                              {item.name}
                            </ListItem>
                        ))}
                    </List>

                </Scrollbars>


            </Grid>

            <Grid container className='footer' xs={12} direction='row' alignItems='center' justify='flex-end'>
                <i className="fas fa-chevron-down fa-2x" style={{color: '#979797'}}/>
            </Grid>

        </Grid>

    )
}
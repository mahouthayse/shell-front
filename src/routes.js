import React from "react";
import { BrowserRouter, Switch, Route, Redirect, useHistory } from 'react-router-dom';
import {Provider} from "react-redux";
import Login from "./pages/login";
import Home from "./pages/home";
import Well from "./pages/well";
import store from "./store";
import baseDashboard from "./components/HOC/baseDashboard";
import Equipments from "./pages/Equipments";
import { isAuthenticated } from './auth';
import Start from "./pages/Start";
import CreateWell from "./pages/CreateWell";
import EditWell from "./pages/EditWell";

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => isAuthenticated() ? ( <Component {...props} />) : ( <Redirect to={{ pathname: "/home", state: {from: props.location}}}/>)} />
);

const Routes = () => (
    <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Login} />
                <PrivateRoute path="/home" component={Start}/>
                <PrivateRoute path="/dashboard" component={baseDashboard(Home)}/>
                <PrivateRoute path="/equipments" component={baseDashboard(Equipments)}/>
                <PrivateRoute path="/select-well" component={baseDashboard(Well)}/>
                <PrivateRoute path="/create-well" component={baseDashboard(CreateWell)}/>
                <PrivateRoute path="/edit-well" component={baseDashboard(EditWell)}/>
            </Switch>
        </BrowserRouter>
    </Provider>
);

export default Routes;

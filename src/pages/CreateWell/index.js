import React, {useEffect, useState, setState, Fragment} from 'react';
import {Grid, Button, Radio, RadioGroup, FormControlLabel, Dialog, DialogContent, Box, IconButton, Divider, Collapse, Typography, CardContent} from "@material-ui/core";
import "./style.scss";
import {useDispatch, useSelector} from "react-redux";
import api from "../../services/api";


export default function CreateWell({history}){
    const dispatch = useDispatch();
    const { equipment } = useSelector( state => (state.equipment));
    const {equipmentName, grade, length, weight, internalDiameter, externalDiameter, materialType} = equipment;
    const [equipments, setEquipments] = useState([]);
    let equipamentos = [];
    let newEquipment = {};
    const [wellName, setWellName] = useState('');
    const [engineer, setEngineer] = useState('');
    const [field, setField] = useState('');
    const [joinVenture, setJoinVenture] = useState('');
    const [startDate, setStartDate] = useState('');
    const [drillingDate, setDrillingDate] = useState('');
    const [endDate, setEndDate] = useState('');
    const [dialogOpen, setDialogOpen] = useState(false);
    const [status, setStatus] = useState('0');
    let wellId = '';

    async function getModelEquipments(){
        let model = localStorage.getItem('modelId');
        console.log(model);
        const response = api.get(`/generic-models/${model}`).then(response => {
            console.log(response.data)
            response.data.map( index => {
                delete index.id;
                equipamentos.push(index);
            });
            setEquipments(equipamentos);
        })
    }

    useEffect(() => {
        getModelEquipments()
    }, []);


    async function createNewWell(){
        const token = localStorage.getItem('authorization');
        const options = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        };

        const response = api.post('/wells/', {
            name: wellName,
            field: field,
            join_venture: joinVenture,
            start_date: startDate,
            drilling: drillingDate,
            end_date: endDate,
            type: 'Pré-Sal 5',
            equipmentsList: equipments
        }, options). then( response =>{
            console.log(response.data);
            wellId = response.data.well.id;
            console.log(wellId);
            localStorage.setItem('wellId', wellId);

            history.push('/equipments')
            }
        )

    }

    function deleteRow(index){
        equipments.splice(index, 1);
        let newArrayEquipments = equipments.slice();
        setEquipments(newArrayEquipments);
    }

    function handleDialogOpen(){
        setDialogOpen(true);
    }


    function handleDialogClose(){
        setDialogOpen(false);
    }


    function addEquipment(){
console.log('ola')
        console.log(equipmentName);

        newEquipment = {
            material_description: equipmentName,
            length: length,
            internal_diameter: internalDiameter,
            external_diameter: externalDiameter,
            weight: weight,
            grade: grade,
            material_type: materialType
        }

        equipments.push(newEquipment);
        let newArrayEquipments = equipments.slice();
        setEquipments(newArrayEquipments);
        setDialogOpen(false);

    }


    return(
        <Grid container className="create-well-container" xs={12} direction='column' alignItems='flex-start' justify='flex-start' style={{minHeight:'100%', padding: 16}}>

            <Grid container className="create-well-content" xs={9} style={{marginBottom: '16px'}}>

                <Grid container xs={12} direction='row' alignItems='center'>
                <h1 style={{textAlign: 'left', marginRight: '16px'}}>New Well</h1>
                    <h2>Basic Information</h2>
                </Grid>

                <Grid container xs={12} direction='row' justify='space-between' alignItems='flex-start'>

                    <Grid container xs={6} direction='row' alignItems='flex-start' justify='flex-start' style={{maxWidth:'48%'}}>

                        <Grid container xs={12} direction='row' justify='space-between' alignItems='center'  style={{marginBottom: '16px'}}>
                            <label htmlFor="name">Well Name</label>
                            <input id="name" required value={wellName} onChange={e => setWellName(e.target.value)}/>
                        </Grid>

                        <Grid container xs={12} direction='row' justify='space-between' alignItems='center'  style={{marginBottom: '16px'}}>
                            <label htmlFor="field">Field</label>
                            <input id="field" value={field} onChange={e => setField(e.target.value)}/>
                        </Grid>
                        <Grid container xs={12} direction='row' justify='space-between' alignItems='center'  style={{marginBottom: '16px'}}>
                            <label htmlFor="venture">Join Venture</label>
                            <input type='text' id="venture" value={joinVenture} onChange={e => setJoinVenture(e.target.value)}/>
                        </Grid>

                    </Grid>

                    <Grid container xs={6} direction='row' alignItems='flex-start' justify='flex-start' style={{maxWidth:'48%'}}>

                        <Grid container xs={12} direction='row' justify='space-between' alignItems='center' style={{marginBottom: '16px'}}>
                            <label htmlFor="engineer">Engineer</label>
                            <input type='text' id="engineer" value={engineer} onChange={e => setEngineer(e.target.value)}/>
                        </Grid>

                        <Grid container xs={12} direction='row' justify='space-between' alignItems='center' style={{marginBottom: '16px'}}>
                            <label htmlFor="startDate">Start Date</label>
                            <input type='date' id="startDate" value={startDate} onChange={e => setStartDate(e.target.value)}/>
                        </Grid>

                        <Grid container xs={12} direction='row' justify='space-between' alignItems='center' style={{marginBottom: '16px'}}>
                            <label htmlFor="drillingDate">Drilling</label>
                            <input type='date' id="drillingDate" value={drillingDate} onChange={e => setDrillingDate(e.target.value)}/>
                        </Grid>

                        <Grid container xs={12} direction='row' justify='space-between' alignItems='center' style={{marginBottom: '16px'}}>
                            <label htmlFor="endDate">End Date</label>
                            <input type='date' id="endDate" value={endDate} onChange={e => setEndDate(e.target.value)}/>
                        </Grid>


                    </Grid>

                </Grid>


            </Grid>

            <Grid container className='create-well-content' xs={9} direction='column'>
                <h2>Well Equipment</h2>

                <table style={{width: '95%'}}>
                    {equipments.map((equipment, index) => {

                        return(
                            <div style={{width: '100%'}}>
                            <tr key={equipment.id} aria-expanded={(status === equipment.id) ? true : false}>
                                <td style={{width: '5%'}}>
                                    <center>
                                        <IconButton onClick={ () => setStatus(equipment.id)} ><i
                                            className="fas fa-plus fa-lg"/></IconButton>
                                    </center>

                                </td>
                                <td style={{width: '50%'}}><input type='text' style={{width: '90%'}} value={equipment.material_description} onChange={e => dispatch({ type: 'setEquipmentName', equipmentName: e.target.value})}/></td>
                                <td style={{width: '10%'}}><center><input type='text' style={{width: '90%'}} value={equipment.external_diameter} onChange={e => dispatch({ type: 'setExternalDiameter', externalDiameter: e.target.value})}/></center></td>
                                <td style={{width: '10%'}}><center><input type='text'  style={{width: '90%'}} value={equipment.length} onChange={e => dispatch({ type: 'setLength', length: e.target.value})}/></center></td>
                                <td style={{width: '5%'}}>
                                    <center>
                                        <button onClick={() => deleteRow(index)}><i className="far fa-trash-alt fa-lg"/>
                                        </button>
                                    </center>
                                </td>

                            </tr>

                                <Collapse in={(status === equipment.id) ? true : false} timeout="auto" unmountOnExit>
                                    <Grid container xs={12} style={{width: '100%'}} direction='row' justify='flex-start' alignItems='flex-start'>

                                        <Grid container xs={12} md={4}>

                                            <RadioGroup value={equipment.material_type} onChange={e => dispatch({ type: 'setMaterialType', materialType: e.target.value})}>
                                                <FormControlLabel value={"CSG"} control={<Radio />} label="Casing" />
                                                <FormControlLabel value={"linear"} control={<Radio />} label="Linear" />
                                            </RadioGroup>

                                        </Grid>

                                        <Grid container xs={12} md={4}>

                                        <Grid container xs={12} direction='row' justify='space-between' alignItems='center' style={{marginBottom: '16px'}}>
                                            <label htmlFor="grade">Grade</label>
                                            <input type='text' id="grade" value={equipment.grade} onChange={e => dispatch({ type: 'setGrade', grade: e.target.value})}/>
                                        </Grid>

                                        <Grid container xs={12} direction='row' justify='space-between' alignItems='center' style={{marginBottom: '16px'}}>
                                            <label htmlFor="diameter">ID</label>
                                            <input type='text' id="diameter" value={equipment.internal_diameter} onChange={e => dispatch({ type: 'setInternalDiameter', internalDiameter: e.target.value})}/>
                                        </Grid>

                                        </Grid>

                                        <Grid container xs={12} md={4}>
                                            <Grid container xs={12} direction='row' justify='space-between' alignItems='center' style={{marginBottom: '16px'}}>
                                                <label htmlFor="weight">Weight</label>
                                                <input type='text' id="weight" value={equipment.weight} onChange={e => dispatch({ type: 'setWeight', weight: e.target.value})}/>
                                            </Grid>
                                        </Grid>

                                    </Grid>
                                </Collapse>
                            </div>
                            )




                    })}

                </table>


                <Grid container xs={12} direction='column' alignItems='flex-start' justify='flex-start' style={{marginTop: '32px'}}>
                    <RadioGroup>
                        <FormControlLabel value="true" control={<Radio />} label="Add these itens to Respot and Relief kits" />
                    </RadioGroup>

                    <Button onClick={handleDialogOpen}>Add Equipment</Button>
                    <Dialog className="dialog-content" open={dialogOpen} onClose={handleDialogClose}>
                        <Box className="dialog-header">
                            <IconButton aria-label="close" onClick={handleDialogClose}><i className="fas fa-times"/></IconButton>
                        </Box>
                        <DialogContent>

                            <h1> New Item</h1>
                            <Divider component={'hr'}/>

                            <Grid container className='dialog-container' xs={12} direction='row' justify='space-between'>

                            <Grid container xs={6} direction='row'>
                                <Grid container xs={12} direction='row' justify='space-between' alignItems='center' style={{marginBottom: '16px'}}>
                                    <label htmlFor="equipment">Equipment</label>
                                    <input type='text' id="equipment" value={equipmentName} onChange={e => dispatch({ type: 'setEquipmentName', equipmentName: e.target.value})}/>
                                </Grid>

                                <Grid container xs={12} direction='row' justify='space-between' alignItems='center' style={{marginBottom: '16px'}}>
                                    <label htmlFor="length">Length</label>
                                    <input type='text' id="length" value={length} onChange={e => dispatch({ type: 'setLength', length: e.target.value})}/>
                                </Grid>

                                <Grid container xs={12} direction='row' justify='space-between' alignItems='center' style={{marginBottom: '16px'}}>
                                    <label htmlFor="internalDiameter">ID</label>
                                    <input type='text' id="internalDiameter" value={internalDiameter} onChange={e => dispatch({ type: 'setInternalDiameter', internalDiameter: e.target.value})}/>
                                </Grid>

                            </Grid>

                            <Grid container xs={6} direction='row'>
                                <Grid container xs={12} direction='row' justify='space-between' alignItems='center' style={{marginBottom: '16px'}}>
                                    <label htmlFor="grade">Grade</label>
                                    <input type='text' id="grade" value={grade} onChange={e => dispatch({ type: 'setGrade', grade: e.target.value})}/>
                                </Grid>

                                <Grid container xs={12} direction='row' justify='space-between' alignItems='center' style={{marginBottom: '16px'}}>
                                    <label htmlFor="diameter">OD</label>
                                    <input type='text' id="diameter" value={externalDiameter} onChange={e => dispatch({ type: 'setExternalDiameter', externalDiameter: e.target.value})}/>
                                </Grid>

                                <Grid container xs={12} direction='row' justify='space-between' alignItems='center' style={{marginBottom: '16px'}}>
                                    <label htmlFor="weight">Weight</label>
                                    <input type='text' id="weight" value={weight} onChange={e => dispatch({ type: 'setWeight', weight: e.target.value})}/>
                                </Grid>

                                <RadioGroup value={materialType} onChange={e => dispatch({ type: 'setMaterialType', materialType: e.target.value})}>
                                    <FormControlLabel value={"CSG"} control={<Radio />} label="Casing" />
                                    <FormControlLabel value={"linear"} control={<Radio />} label="Linear" />
                                </RadioGroup>
                            </Grid>

                            </Grid>

                            <button onClick={handleDialogClose}> Cancel</button>
                            <button onClick={addEquipment}> Save</button>

                        </DialogContent>




                    </Dialog>



                </Grid>


                <Grid container xs={12} direction='row' alignItems='flex-start' justify='flex-end' style={{marginTop: '32px'}}>
                    <Button onClick={() => history.push('/home')}>Cancel</Button>
                    <Button onClick={createNewWell}>Next</Button>
                </Grid>




            </Grid>


        </Grid>

    )
}
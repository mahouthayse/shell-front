import React from "react";
import {Grid, Button, TableRow, TableBody, Table, TableHead, TableContainer, TableCell} from "@material-ui/core";
import "react-sweet-progress/lib/style.css";
import "./style.scss";
function createData(date, message) {
    return { date, message };
}

const rows = [
    createData('Just Now', 'Well D required 500" Casing 30 T-85 64,5 lb/ft from your Respot Kit please confirm'),
    createData('12 Jun', 'Well A reserved 500" Casing 30\' T-95 64,5 lb/ft from Well B request ')
];


export default function Home(){

    return(
       <Grid container className="page-container-wrapper" xs={12} direction="row" justify="space-around" alignItems="center">

               <Grid container className="page-container-row" xs={12} >

                   <Grid container className="page-container-column" xs={12} md={12} style={{minHeight: 300}}>
                       <h1> Gráfico</h1>
                   </Grid>
               </Grid>

           <Grid container className="page-container-row" xs={12} direction="row" justify="space-between" alignItems="flex-start">

               <Grid item className="page-container-column" xs={8} direction="column" justify="flex-start" alignItems="flex-start" style={{maxWidth:'65%', minHeight: 300}}>
                    <h1> Notifications</h1>

                   <TableContainer component={'table'}>
                       <Table size="small" aria-label="a dense table">
                           <TableBody>
                               {rows.map((row) => (
                                   <TableRow key={row.date}>
                                       <TableCell component="th" scope="row">
                                           <span> <i className="far fa-clock"/> {row.date}</span>
                                       </TableCell>
                                       <TableCell align="left"><span>{row.message}</span></TableCell>
                                   </TableRow>
                               ))}
                           </TableBody>
                       </Table>
                   </TableContainer>

               </Grid>

               <Grid item className="page-container-column" xs={4} direction="column" justify="space-between" alignItems="flex-start"  style={{minHeight: 300}}>
                   <h1> Timeline informations</h1>
                   <Button> Export to schedule</Button>
                   <Button>Export all 'To buy' list</Button>

               </Grid>

           </Grid>

       </Grid>

    )
}

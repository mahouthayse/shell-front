import React from 'react';
import {Grid, Divider, Paper, Button} from '@material-ui/core';
import "./style.scss";
import SelectModel from "../../components/SelectModel";



export default function Well({history}){
    return(
        <Grid container xs={12} direction='row' alignItems='flex-start' justify='flex-start' style={{minHeight:'100%', padding: 16}}>

            <Grid container className='well-container' xs={12} sm={8} md={7} direction='row' alignItems='flex-start' style={{backgroundColor: '#fff'}}>
                <h1 style={{width: '30%', textAlign: 'left'}}>New Well</h1>
                <Grid container xs={12} direction='row' alignItems='flex-start' justify='space-between'>
                    <SelectModel/>
                </Grid>

                <Grid container xs={12} direction='row' alignItems='flex-start' justify='flex-end' style={{marginTop: '32px'}}>
                    <Button onClick={() => history.push('/create-well')}>From scratch</Button>
                    <Button onClick={() => history.push('/home')}>Cancel</Button>
                    <Button onClick={() => history.push('/create-well')}>Next</Button>
                </Grid>

            </Grid>
        </Grid>
    )
}
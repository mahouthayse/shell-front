import React, {useState} from "react";
import {Grid} from "@material-ui/core";
import "./style.scss";
import Logotipo from "../../assets/logotipo.png";
import {Link, useHistory} from "react-router-dom";
import { useDispatch, useSelector} from "react-redux";
import api from "../../services/api";



export default function Login(){
    const { user } = useSelector( state => (state.user));
    const {email, password} = user;
    const dispatch = useDispatch();
    const [ err, setErr] = useState(false);
    const history = useHistory();



    async function handleSubmitLogin(event){
        if(!email || !password){
            setErr('Por favor, prencha todas as informações solicitadas!');
        } else {
            api.post('users/login', {
                email: email,
                password: password
            })
                .then(function (response) {
                    console.log(response);
                    if(response.status === 200){
                        localStorage.setItem('authorization', response.data.token);
                        localStorage.setItem('id', response.data.id);
                        localStorage.setItem('name', response.data.name);
                        history.push('/home');
                    } else {
                        setErr('Você não tem permissão para acessar essa área!');
                    }
                })
                .catch(function (error) {
                    console.log(error)
                    setErr('Usuário e/ou senha inválido(s)!');
                });
        }

    }


    return(
        <Grid container className="login-container" xs={12} direction="row" justify="center" alignItems="center">
            <Grid container className="login-content-wrapper" sm={5} md={5} lg={2} direction="row" justify="center" alignItems="flex-end">
                <img className="image-logo" src={Logotipo}/>

                <Grid container className="login-content-form" xs={12} direction="column" >
                    <input className="login-input" id="email" type="text" placeholder="email" required value={email} onChange={e => dispatch({ type: 'setEmail', email: e.target.value})}/>
                    <input className="login-input" id="password" type="password" placeholder="password" required value={password} onChange={e => dispatch({ type: 'setPassword', password: e.target.value})}/>
                    <Link to="/" className="login-link" >Forgot password?</Link>
                    <button className="button-login btnCursor" onClick={handleSubmitLogin}>Login</button>
                </Grid>

                <Grid container className="login-content-bottom" xs={12} direction="column" justify="center" alignItems="center">
                    <p style={{fontSize: '1.25rem'}}><i className="fas fa-headset"></i> <strong>Support</strong></p>
                    <p>contact@i2wm.com</p>
                    <p>+8801829193637</p>
                    <p style={{color: '#fff'}}>www.i2wm.com</p>
                </Grid>


            </Grid>
        </Grid>

    )
}
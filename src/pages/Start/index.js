import React, { useEffect, useState, setState} from 'react';
import { Grid, Button} from "@material-ui/core";
import "./style.scss";
import Logo from "../../assets/logotipo.png";
import api from "../../services/api";


export default function Start({ history}){
    const [disableWell, setDisableWell] = useState('')
    const [disableEquipments, setDisableEquipments] = useState('')

    async function getWell(){
        let userId = localStorage.getItem('id');
        console.log(userId);
        const response = api.get(`/users/${userId}`).then(response => {
            console.log(response.data);

            if(response.data.well_id !== null){
                setDisableWell(true);
                setDisableEquipments(false);
                let wellId = response.data.well_id;
                localStorage.setItem('wellId', wellId);
            } else{
                setDisableWell(false);
                setDisableEquipments(true);
            }

        })
    }

    useEffect(() => {
        getWell()
    }, []);



    return(

        <Grid container className="start-wrapper" xs={12} direction='row' alignItems='center' justify='center'>

            <Grid container xs={12} direction='column' alignItems='center' justify='center'>
                <img src={Logo}/>
            </Grid>

            <Grid container xs={6} direction='row' alignItems='center' justify='space-between'>
                <Button onClick={() => history.push('/select-well')} disabled={ (disableWell === false) ? false : true}> New Well</Button>
                <Button onClick={() => history.push('/equipments')} disabled={ (disableEquipments === false) ? false : true}> My Equipments</Button>
                <Button onClick={() => history.push('/dashboard')}> Timeline</Button>
            </Grid>

            <Grid container xs={12} direction='column' alignItems='center' justify='center'>
                <p><i className="far fa-question-circle"/> Tutorial </p>
                <p><i className="fas fa-headset"/> Support </p>
            </Grid>


        </Grid>

    )
}
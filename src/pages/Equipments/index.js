import React, {useEffect, useState} from 'react';
import {
    Grid,
    Paper,
    Switch,
    Button,
    ExpansionPanel,
    ExpansionPanelSummary,
    Dialog,
    ExpansionPanelDetails,
    RadioGroup,
    Radio,
    FormControlLabel,
} from '@material-ui/core';
import "./style.scss";
import moment from 'moment';
import api from "../../services/api";


export default function Equipments({history}){
    const token = localStorage.getItem('authorization');
    const options = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    };
    const [dialogOpen, setDialogOpen] = useState(false);
    const [dialogOpenRelief, setDialogOpenRelief] = useState(false);
    const [dialogOpenRespot, setDialogOpenRespot] = useState(false);
    const [requestDialogOpen, setRequestDialogOpen] = useState(false);
    const [disableWell, setDisableWell] = useState('');
    const [primaryKit, setPrimaryKit] = useState([]);
    const [reliefKit, setReliefKit] = useState([]);
    const [respostKit, setRespostKit] = useState([]);
    const [engineer, setEngineer] = useState(localStorage.getItem('name'));
    const [field, setField] = useState('');
    const [venture, setVenture] = useState('');
    const [name, setName] = useState('');
    const [startDate, setStartDate] = useState('');
    const [drillingDate, setDrillingDate] = useState('');
    const [endDate, setEndDate] = useState('');
    let dateString = '';
    const [dialogCaseName, setDialogCaseName] = useState('');
    const [dialogCaseLength, setDialogCaselength] = useState('');
    const [dialogCaseId, setDialogCaseId] = useState('');
    const [wellEquipmentList, setWellEquipmentList] = useState([]);
    const [newWellEquipmentList, setNewWellEquipmentList] = useState([]);
    const [notifications, setNotifications] = useState([]);
    const [newNotifications, setNewNotifications] = useState([]);
    const [notificationsId, setNotificationsId] = useState([]);
    let newWellList = [];
    const lenghtVariable = [{
        newLength: '',
    }]
    const [lengthReserved, setLengthReserved] = useState(lenghtVariable);
    const [wellId, setwellId] = useState(localStorage.getItem('wellId'));
    let not = [];
    let notId = []



    async function getKits(){
        api.get(`/equipments-kits?well_id=${wellId}`, options).then( response => {
            setPrimaryKit(response.data.equipments.primary_kit);
            setReliefKit(response.data.equipments.relief_kit);
            setRespostKit(response.data.equipments.respot_kit);
        }).then( () =>{
            api.get(`/wells/${wellId}`, options).then( response => {
                setName(response.data.name);
                setField(response.data.field);
                setVenture(response.data.join_venture);

                dateString = response.data.start_date;
                var date = moment(dateString);
                var dateComponent = date.utc().format('DD/MM/YYYY');
                setStartDate(dateComponent);
                dateString = response.data.drillingDate;
                date = moment(dateString);
                dateComponent = date.utc().format('DD/MM/YYYY');
                setDrillingDate(dateComponent);
                dateString = response.data.end_date;
                date = moment(dateString);
                dateComponent = date.utc().format('DD/MM/YYYY');
                setEndDate(dateComponent);
            })
        })
    }

    async function getNotifications(){
        api.get('/notifications', options).then( response =>{
            response.data.map( index => {
               not.push(index);
               notId.push(index.equipment_id_required);
            })
            setNotifications(not);
            setNotificationsId(notId);
        })

    }

    useEffect(() => {
        getKits();
        getNotifications()
    }, []);


    function updateUseRelief(obj, indice, value) {
        let eq = [...reliefKit];
        eq[indice].in_use = value;
        setReliefKit(eq);
        // console.log(eq);

        api.put(`/equipments-kits?well_id=${wellId}`, [
            {
                equipment_id: reliefKit[indice].id,
                kit: "relief_kit",
                in_use: reliefKit[indice].in_use
            }
        ], options)

    }

    function updateUseRespot(obj, indice, value) {
        let eq = [...respostKit];
        eq[indice].in_use = value;
        setRespostKit(eq);

        api.put(`/equipments-kits?well_id=${wellId}`, [
            {
                equipment_id: respostKit[indice].id,
                kit: "respot_kit",
                in_use: respostKit[indice].in_use
            }
        ], options)


    }



    async function deleteWell(){
        api.delete(`/wells/${wellId}`, options).then( response =>{
            console.log(response);
            setDisableWell(true)
        })
    }

    function handleDialogOpen(obj, indice){
        setDialogOpen(true);
        let eq = [...primaryKit];
        let eqName = eq[indice].joined_equipment_name;
        let newEqName = eqName.replace("'", "");
        console.log(newEqName);
        setDialogCaseName(eqName);
        let eqLength = eq[indice].length;
        setDialogCaselength(eqLength);
        let eqId = eq[indice].id;
        setDialogCaseId(eqId);

        api.post('/requests/index',  {
            joinedStringEquipment: newEqName
        }, options).then( response =>{
            console.log(response);
            response.data.map( index =>{
                newWellList.push(index)
            })
            setWellEquipmentList(newWellList);
        })
    }

    function handleDialogOpenRelief(obj, indice){
        setDialogOpenRelief(true);
        let eq = [...reliefKit];
        let eqName = eq[indice].joined_equipment_name;
        let newEqName = eqName.replace("'", "");
        console.log(newEqName);
        setDialogCaseName(eqName);
        let eqLength = eq[indice].length;
        setDialogCaselength(eqLength);
        let eqId = eq[indice].id;
        setDialogCaseId(eqId);

        api.post('/requests/index',  {
            joinedStringEquipment: newEqName
        }, options).then( response =>{
            console.log(response);
            response.data.map( index =>{
                newWellList.push(index)
            })
            setWellEquipmentList(newWellList);
        })
    }

    function handleDialogOpenRespot(obj, indice) {
        setDialogOpenRespot(true);
        let eq = [...respostKit];
        let eqName = eq[indice].joined_equipment_name;
        let newEqName = eqName.replace("'", "");
        console.log(newEqName);
        setDialogCaseName(eqName);
        let eqLength = eq[indice].length;
        setDialogCaselength(eqLength);
        let eqId = eq[indice].id;
        setDialogCaseId(eqId);

        api.post('/requests/index', {
            joinedStringEquipment: newEqName
        }, options).then(response => {
            console.log(response);
            response.data.map(index => {
                newWellList.push(index)
            })
            setWellEquipmentList(newWellList);
        })
    }



    function addLength(obj, indice, value, key) {
        let eq = [...wellEquipmentList];
        let newEq = eq[indice];
        setNewWellEquipmentList(newEq);
        console.log(newEq)
        let le = [...lengthReserved];
        le[indice].newLength = value;
        setLengthReserved(le);
        console.log(le[0].newLength)
    }

    function handlePostRequest(){
        api.post(`/requests?well_id=${wellId}`, {
            kit_requesting: 'primary_kit',
            equipment_id_missing: dialogCaseId,
            wells_requested: [
                {
                    well_id: newWellEquipmentList.well_id,
                    owner_kit_name: newWellEquipmentList.kit,
                    equipment_id:newWellEquipmentList.equipment_id,
                    length: lengthReserved[0].newLength
                }
            ]


        }, options).then( response =>{
            setDialogOpen(false);
            setDialogOpenRelief(false);
            setDialogOpenRespot(false);
        })
    }




    function handleDialogClose(){
        setDialogOpen(false);
    }

        function handleDialogCloseRelief(){
            setDialogOpenRelief(false);
        }

        function handleDialogCloseRespot(){
            setDialogOpenRespot(false);
        }

        function handleRequestDialog(indice){
            setRequestDialogOpen(true);
            let not = notifications[indice];
            setNewNotifications(not);
        }

        function handleAcceptRequest(){
        let id = newNotifications.id;
        console.log(id)
        api.post(`requests/accepted/${id}`, null, options);
        setRequestDialogOpen(false);
        }



    return(
        <Grid container className='equipments-container' xs={12} direction='row' alignItems='flex-start' justify='flex-start' style={{padding:16}}>

            <Grid container className='table-container' xs={9} direction='row' alignItems='flex-start' justify='flex-start' style={{height: 600, padding: 16, backgroundColor: '#fff', overflow: 'auto'}}>

                <Grid container className='well-header' xs={12} direction='row' alignItems='flex-start' justify='flex-start'>

                    <Grid container xs={3} direction='column' alignItems='flex-start' justify='flex-start'>
                        <h1> {name} </h1>
                    </Grid>

                    <Grid container xs={5} direction='column'>
                            <span>Engineer: {engineer}</span>
                            <span>Field: {field}</span>
                            <span>Venture: {venture}</span>
                    </Grid>

                    <Grid container xs={4} direction='column'>
                            <span>Start Date: {startDate}</span>
                            <span>Drilling Start: {drillingDate}</span>
                            <span>End Date: {endDate}</span>
                    </Grid>

                </Grid>



        <Grid container xs={12} direction='column'>
            <ExpansionPanel expanded={true}>
                <ExpansionPanelSummary expandIcon={<i className="fas fa-plus"></i>} aria-controls="panel1a-content" id="panel1a-header" style={{backgroundColor: '#9E241C', color: '#fff'}}>
                    <span><strong>Primary Kit</strong></span>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>

                    <table style={{width: 720}}>
                        <tr>
                            <th style={{textAlign: 'left'}}>Equipment</th>
                            <th style={{textAlign: 'left'}}>Use</th>
                            <th style={{textAlign: 'left'}}>Status</th>
                            <th style={{textAlign: 'left'}}>Request From</th>
                            <th style={{textAlign: 'left'}}>Request By</th>
                        </tr>

                        {primaryKit.map( (kit, index) => (
                            <tr key={kit.id}>
                                <td style={{width: '40%'}}>{kit.joined_equipment_name}</td>
                                <td style={{width: '10%'}}>
                                    <Switch checked={kit.in_use}/>
                                </td>
                                <td style={{width: '10%'}}>{kit.status}</td>
                                <td style={{width: '20%'}}>Request {(kit.in_use === true) ? <button onClick={() => handleDialogOpen(primaryKit, index)}><i className="fas fa-caret-down fa-lg"/></button> :' '} </td>
                                <td style={{width: '20%'}}>Request By{(kit.in_use === false && notificationsId.includes(kit.id)) ? <button onClick={() => handleRequestDialog(index)}><i className="fas fa-caret-down fa-lg"/></button> :' '} </td>
                            </tr>
                        ))}

                    </table>
                </ExpansionPanelDetails>
            </ExpansionPanel>

                <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<i className="fas fa-plus"></i>} aria-controls="panel2a-content" id="panel2a-header" style={{backgroundColor: '#9E241C', color: '#fff'}}>
                        <span><strong>Relief Kit</strong></span>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>

                        <table style={{width: 710}}>
                            <tr>
                                <th style={{textAlign: 'left'}}>Equipment</th>
                                <th style={{textAlign: 'left'}}>Use</th>
                                <th style={{textAlign: 'left'}}>Status</th>
                                <th style={{textAlign: 'left'}}>Request From</th>
                                <th style={{textAlign: 'left'}}>Request By</th>
                            </tr>

                            {reliefKit.map( (kit, index) => (
                                <tr key={kit.id}>
                                    <td style={{width: '40%'}}>{kit.joined_equipment_name}</td>
                                    <td style={{width: '10%'}}>
                                        <Switch checked={kit.in_use} onChange={e => updateUseRelief(kit.in_use, index, !kit.in_use)} name="checkedA"/>
                                    </td>
                                    <td style={{width: '10%'}}>{kit.status}</td>
                                    <td style={{width: '20%'}}>Request {(kit.in_use === true) ? <button onClick={() => handleDialogOpenRelief(reliefKit, index)}><i className="fas fa-caret-down fa-lg"/></button> :' '} </td>
                                    <td style={{width: '20%'}}>Request By{(kit.in_use === false && notificationsId.includes(kit.id)) ? <button onClick={() => handleRequestDialog(index)}><i className="fas fa-caret-down fa-lg"/></button> :' '}</td>
                                </tr>
                            ))}

                        </table>

                    </ExpansionPanelDetails>
                </ExpansionPanel>

                <ExpansionPanel >
                    <ExpansionPanelSummary expandIcon={<i className="fas fa-plus"></i>} aria-controls="panel3a-content" id="panel3a-header" style={{backgroundColor: '#9E241C', color: '#fff'}}>
                        <span><strong>Respot Kit</strong></span>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>

                        <table style={{width: 710}}>
                            <tr>
                                <th style={{textAlign: 'left'}}>Equipment</th>
                                <th style={{textAlign: 'left'}}>Use</th>
                                <th style={{textAlign: 'left'}}>Status</th>
                                <th style={{textAlign: 'left'}}>Request From</th>
                                <th style={{textAlign: 'left'}}>Request By</th>
                            </tr>

                            {respostKit.map( (kit, index) => (
                                <tr key={kit.id}>
                                    <td style={{width: '40%'}}>{kit.joined_equipment_name}</td>
                                    <td style={{width: '10%'}}>
                                        <Switch checked={kit.in_use} onChange={e => updateUseRespot(kit.in_use, index, !kit.in_use)} name="checkedA"/>
                                    </td>
                                    <td style={{width: '10%'}}>{kit.status}</td>
                                    <td style={{width: '20%'}}>Request {(kit.in_use === true) ? <button onClick={() => handleDialogOpenRespot(respostKit, index)}><i className="fas fa-caret-down fa-lg"/></button> :' '} </td>
                                    <td style={{width: '20%'}}>Request By{(kit.in_use === false && notificationsId.includes(kit.id)) ? <button className='button-request' onClick={() => handleRequestDialog(index)}><i className="fas fa-caret-down fa-lg"/></button> :' '}</td>
                                </tr>
                            ))}

                        </table>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
        </Grid>


                <Dialog open={dialogOpen} onClose={handleDialogClose} component={Paper} style={{minWidth:'700px'}}>

                    <Grid container className='dialog-container' xs={12} direction='column' alignItems='flex-start' justify='flex-start' style={{padding:20, minWidth: '550px'}}>
                        <h1 className='dialog-h1'> Request From</h1>

                        <Grid container xs={12} direction='row' alignItems='center' justify='space-between'>

                            <Grid container xs={7} direction='row' justify='space-between' alignItems='center' style={{marginBottom: '16px'}}>
                                <label className='label-dialog-header' htmlFor="equipment">Equipment</label>
                                <input className='input-dialog-header' type='text' id="equipment" value={dialogCaseName} style={{width: '70%', marginRight: 10}}/>
                            </Grid>

                            <Grid container xs={5} direction='row' justify='space-between' alignItems='center' style={{marginBottom: '16px'}}>
                                <label className='label-dialog-header' htmlFor="length">Total Length</label>
                                <input className='input-dialog-header' type='text' id="length" value={dialogCaseLength} style={{width: '55%'}}/>
                            </Grid>

                        </Grid>

                        <Grid container xs={12} direction='column' justify='flex-start' alignItems='flex-start' style={{backgroundColor: '#EDEDED', padding: 10, borderRadius: 5, marginBottom: 15}}>
                            <RadioGroup>

                                <FormControlLabel control={<Radio />} label="Reserved Equipment" />

                                <table className='dialog-table' style={{width: 550}}>
                                    <tr>
                                        <th>Provenance</th>
                                        <th>Venture</th>
                                        <th>Total Available</th>
                                        <th>Length</th>
                                    </tr>

                                    {wellEquipmentList.map( (list, index) => (
                                        <tr key={list.well_id} style={{marginBottom: 5}}>
                                            <td style={{width: '25%'}}>{`${list.well_name} - ${list.kit}`}</td>
                                            <td style={{width: '25%'}}>{list.join_venture}</td>
                                            <td style={{width: '25%'}}>{list.length}</td>
                                            <td style={{width: '25%'}}><input type='text' value={lengthReserved.newLength} name='newLenght' onChange={(e) => addLength(list, index, e.target.value, e.target.newLength)}/></td>
                                        </tr>
                                    ))}

                                </table>
                            </RadioGroup>

                        </Grid>


                        <Grid container className='button-dialog-container' xs={12} direction='row' justify='flex-end' alignItems='flex-start'>
                            <Button className='button-dialog-close' onClick={handleDialogClose}> Cancel</Button>
                            <Button onClick={handlePostRequest}> Save</Button>
                        </Grid>


                    </Grid>

                </Dialog>


                <Dialog open={dialogOpenRelief} onClose={handleDialogCloseRelief} component={Paper} style={{minWidth:'700px'}}>

                    <Grid container className='dialog-container' xs={12} direction='column' alignItems='flex-start' justify='flex-start' style={{padding:20, minWidth: '550px'}}>
                        <h1 className='dialog-h1'> Request From</h1>

                        <Grid container xs={12} direction='row' alignItems='center' justify='space-between'>

                            <Grid container xs={7} direction='row' justify='space-between' alignItems='center' style={{marginBottom: '16px'}}>
                                <label className='label-dialog-header' htmlFor="equipment">Equipment</label>
                                <input className='input-dialog-header' type='text' id="equipment" value={dialogCaseName} style={{width: '70%', marginRight: 10}}/>
                            </Grid>

                            <Grid container xs={5} direction='row' justify='space-between' alignItems='center' style={{marginBottom: '16px'}}>
                                <label className='label-dialog-header' htmlFor="length">Total Length</label>
                                <input className='input-dialog-header' type='text' id="length" value={dialogCaseLength} style={{width: '55%'}}/>
                            </Grid>

                        </Grid>

                        <Grid container xs={12} direction='column' justify='flex-start' alignItems='flex-start' style={{backgroundColor: '#EDEDED', padding: 10, borderRadius: 5, marginBottom: 15}}>
                            <RadioGroup>

                                <FormControlLabel control={<Radio />} label="Reserved Equipment" />

                                <table className='dialog-table' style={{width: 550}}>
                                    <tr>
                                        <th>Provenance</th>
                                        <th>Venture</th>
                                        <th>Total Available</th>
                                        <th>Length</th>
                                    </tr>

                                    {wellEquipmentList.map( (list, index) => (
                                        <tr key={list.well_id} style={{marginBottom: 5}}>
                                            <td style={{width: '25%'}}>{`${list.well_name} - ${list.kit}`}</td>
                                            <td style={{width: '25%'}}>{list.join_venture}</td>
                                            <td style={{width: '25%'}}>{list.length}</td>
                                            <td style={{width: '25%'}}><input type='text' value={lengthReserved.newLength} name='newLenght' onChange={(e) => addLength(list, index, e.target.value, e.target.newLength)}/></td>
                                        </tr>
                                    ))}

                                </table>
                            </RadioGroup>

                        </Grid>


                        <Grid container className='button-dialog-container' xs={12} direction='row' justify='flex-end' alignItems='flex-start'>
                            <Button className='button-dialog-close' onClick={handleDialogCloseRelief}> Cancel</Button>
                            <Button onClick={handlePostRequest}> Save</Button>
                        </Grid>


                    </Grid>

                </Dialog>



                <Dialog open={dialogOpenRespot} onClose={handleDialogCloseRespot} component={Paper} style={{minWidth:'700px'}}>

                    <Grid container className='dialog-container' xs={12} direction='column' alignItems='flex-start' justify='flex-start' style={{padding:20, minWidth: '550px'}}>
                        <h1 className='dialog-h1'> Request From</h1>

                        <Grid container xs={12} direction='row' alignItems='center' justify='space-between'>

                            <Grid container xs={7} direction='row' justify='space-between' alignItems='center' style={{marginBottom: '16px'}}>
                                <label className='label-dialog-header' htmlFor="equipment">Equipment</label>
                                <input className='input-dialog-header' type='text' id="equipment" value={dialogCaseName} style={{width: '70%', marginRight: 10}}/>
                            </Grid>

                            <Grid container xs={5} direction='row' justify='space-between' alignItems='center' style={{marginBottom: '16px'}}>
                                <label className='label-dialog-header' htmlFor="length">Total Length</label>
                                <input className='input-dialog-header' type='text' id="length" value={dialogCaseLength} style={{width: '55%'}}/>
                            </Grid>

                        </Grid>

                        <Grid container xs={12} direction='column' justify='flex-start' alignItems='flex-start' style={{backgroundColor: '#EDEDED', padding: 10, borderRadius: 5, marginBottom: 15}}>
                            <RadioGroup>

                                <FormControlLabel control={<Radio />} label="Reserved Equipment" />

                                <table className='dialog-table' style={{width: 550}}>
                                    <tr>
                                        <th>Provenance</th>
                                        <th>Venture</th>
                                        <th>Total Available</th>
                                        <th>Length</th>
                                    </tr>

                                    {wellEquipmentList.map( (list, index) => (
                                        <tr key={list.well_id} style={{marginBottom: 5}}>
                                            <td style={{width: '25%'}}>{`${list.well_name} - ${list.kit}`}</td>
                                            <td style={{width: '25%'}}>{list.join_venture}</td>
                                            <td style={{width: '25%'}}>{list.length}</td>
                                            <td style={{width: '25%'}}><input type='text' value={lengthReserved.newLength} name='newLenght' onChange={(e) => addLength(list, index, e.target.value, e.target.newLength)}/></td>
                                        </tr>
                                    ))}

                                </table>
                            </RadioGroup>

                        </Grid>


                        <Grid container className='button-dialog-container' xs={12} direction='row' justify='flex-end' alignItems='flex-start'>
                            <Button className='button-dialog-close' onClick={handleDialogCloseRespot}> Cancel</Button>
                            <Button onClick={handlePostRequest}> Save</Button>
                        </Grid>

                    </Grid>


                </Dialog>


                <Dialog open={requestDialogOpen} onClose={handleDialogClose} component={Paper} style={{minWidth:'700px'}}>

                    <Grid container className='dialog-container' xs={12} direction='column' alignItems='flex-start' justify='flex-start' style={{padding:20, minWidth: '550px'}}>
                        <p> You have a request from {newNotifications.well_sender}</p>


                        <Grid container className='button-dialog-container' xs={12} direction='row' justify='flex-end' alignItems='flex-start'>
                            <Button className='button-dialog-close' onClick={ () => handleAcceptRequest()}> Accept</Button>
                            <Button className='button-dialog-close' onClick={handleDialogClose}> Cancel</Button>
                        </Grid>


                    </Grid>

                </Dialog>



            </Grid>

            <Grid container className='buttons-container' xs={3} direction='column'>
                <Button onClick={() => history.push('/edit-well')}>Edit Well Informations</Button>
                <Button onClick={deleteWell} disabled={ (disableWell === false) ? true : false} >Delete Well </Button>

            </Grid>
        </Grid>
    )
}